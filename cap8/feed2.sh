#!/bin/bash
# feed2.sh
#
# Extract headlines
#
# Example: bash feed2.sh http://br-linux.org/feed.xml

lynx -source "$1" | 
	grep '<title>' |
	sed 's/^.\{1,18\}//g ; s/\].*//g ; s/&#36;/$/g ; 1d'
