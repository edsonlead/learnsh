#!/bin/bash
# feed.sh
#
# Extract headlines

# lynx -source http://br-linux.org/feed.xml

grep '<title>' brlinux.xml | sed 's/^.\{1,18\}//g ; s/\].*//g ; s/&#36;/$/g ; 1d'
