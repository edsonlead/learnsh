#!/bin/bash
# users-v0.2.sh
#
# show login and name user system
# reads file data /etc/passwd
#
# adapted by Edson Silva
# Version 1: show users and names separated by TAB
# Version 2: add -h option
# Version 3: add -v and invalid option
# Version 4: bug fixed, add basename, update -v, add --help and --version options
# Version 5: add -s and --sort options

sort=0		# It should be sort?

MESSAGE_USE="
Use: $(basename "$0") [-h | -v | -s]

	-s, --sort			sort the list
	-h, --help			show this help screen and exit
	-v, --version			show the version of the software and exit

"
# Treatment of command line options
case "$1" in
	-s | --sort) sort=1	;;
	-h | --help) echo "$MESSAGE_USE" && exit 0 ;;
	-v | --version) echo -n $(basename "$0")
		grep '^# Version ' "$0" | tail -1 | cut -d : -f 1 | tr -d \#
		exit 0
		;;
	*) if test -n "$1"
		then
			echo Invalid Option: $1 
			exit 1
		fi	
	;;
esac

# Extract the listing
list=$(cut -d : -f 1,5 /etc/passwd)

# sort list (if necessary)
if test "$sort" = 1
then
	list=$(echo "$list" | sort)
fi

# Processing
echo "$list" | tr : \\t
