#!/bin/bash
# users-v0.3.sh
#
# show login and name user system
# reads file data /etc/passwd
#
# adapted by Edson Silva
# Version 1: show users and names separated by TAB
# Version 2: add -h option
# Version 3: add -v and invalid option
# Version 4: bug fixed, add basename, update -v, add --help and --version options
# Version 5: add -s and --sort optionsi
# Version 6: add -r, --reverse, -u, --upercase and read multiple options
# Version 7: add -d and --delimiter options, update (optimize) code

sort=0		# It should be sort?
reverse=0	# It should be reverse?
uppercase=0	# It should be uppercase?
delim='\t'	# Character used as the delimiter

MESSAGE_USE="
Use: $(basename "$0") [OPTIONS]
	-d, --delimiter C		use the character C as delimiter
	-r, --reverse			reverse the listing
	-s, --sort			sort the list
	-u, --uppercase			show the list in uppercase
	-h, --help			show this help screen and exit
	-v, --version			show the version of the software and exit

"
# Treatment of command line options
while test -n "$1"
do
	case "$1" in
		# Options that on/off keys
		-s | --sort) sort=1	;;
		-r | --reverse) reverse=1 ;;
		-u | --uppercase) uppercase=1 ;;
		
		-d | --delimiter)
			shift
			delim="$1"

			if test -z "$delim"
			then
				echo "Missing argument for -d"
				exit 1
			fi
			;;

		-h | --help)
			echo "$MESSAGE_USE"
			exit 0
			;;

		-v | --version) 
			echo -n $(basename "$0")
			grep '^# Version ' "$0" | tail -1 | cut -d : -f 1 | tr -d \#
			exit 0
			;;

		*)
			echo Invalid Option: $1 
			exit 1
			;;
	esac

	# Option $1 already processed. The queue should go
	shift
done

# Extract the listing
list=$(cut -d : -f 1,5 /etc/passwd)

# Sort, reverse or convert to uppercase (if necessary)
test "$sort" = 1 && list=$(echo "$list" | sort)
test "$reverse" = 1 && list=$(echo "$list" | tac)
test "$uppercase" = 1 && list=$(echo "$list" | tr a-z A-Z)

# Processing
echo "$list" | tr : "$delim"
