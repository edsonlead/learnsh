#!/bin/bash
# users-v0.3.getopts.sh
#
# show login and name user system
# reads file data /etc/passwd
#
# adapted by Edson Silva
# Version 1: show users and names separated by TAB
# Version 2: add -h option
# Version 3: add -v and invalid option
# Version 4: bug fixed, add basename, update -v, add --help and --version options
# Version 5: add -s and --sort optionsi
# Version 6: add -r, --reverse, -u, --upercase and read multiple options
# Version 7: add -d and --delimiter options, update (optimize) code
# Version 7g: change code for use "getopts"

sort=0		# It should be sort?
reverse=0	# It should be reverse?
uppercase=0	# It should be uppercase?
delim='\t'	# Character used as the delimiter

MESSAGE_USE="
Use: $(basename "$0") [OPTIONS]

	OPTIONS:
	-d C			use the character C as delimiter
	-r			reverse the listing
	-s			sort the list
	-u			show the list in uppercase

	-h			show this help screen and exit
	-v			show the version of the software and exit

"
# Treatment of command line options
while getopts ":hvd:rsu" option
do
	case "$option" in
		# Options that on/off keys
		s) sort=1	;;
		r) reverse=1 ;;
		u) uppercase=1 ;;
		
		d)
			delim="$OPTARG"
			;;

		h)
			echo "$MESSAGE_USE"
			exit 0
			;;

		v) 
			echo -n $(basename "$0")
			grep '^# Version ' "$0" | tail -1 | cut -d : -f 1 | tr -d \#
			exit 0
			;;

		\?)
			echo Invalid Option: $OPTARG 
			exit 1
			;;

		:)
			echo Missing argument for: $OPTARG
			exit 1
			;;
	esac

done

# Extract the listing
list=$(cut -d : -f 1,5 /etc/passwd)

# Sort, reverse or convert to uppercase (if necessary)
test "$sort" = 1 && list=$(echo "$list" | sort)
test "$reverse" = 1 && list=$(echo "$list" | tac)
test "$uppercase" = 1 && list=$(echo "$list" | tr a-z A-Z)

# Processing
echo "$list" | tr : "$delim"
