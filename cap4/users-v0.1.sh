#!/bin/bash
# users-v0.1.sh
#
# show login and name user system
# reads file data /etc/passwd
#
# adapted by Edson Silva
# Version 1: show users and names separated by TAB
# Version 2: add -h option
# Version 3: add -v and invalid option

MESSAGE_USE="
Use: $0 [-h | -v]

	-h		show this help screen and exit
	-v		show the version of the software and exit

"
# Treatment of command line options
case "$1" in
	-h) echo "$MESSAGE_USE" && exit 0;;
	-v) echo $0 Version 3 && exit 0;;
	*) if test -n "$1"
		then
			echo Invalid Option: $1 
			exit 1
		fi	
	;;
esac


# Processing
cut -d : -f 1,5 /etc/passwd | tr : \\t
